package io.shaneconnolly.dev.eeetest.core.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Waiter {

    @Autowired
    private WebDriverWait defaultWait;

    public void waitForElementBy(By by){
        defaultWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    public void waitForElementToBeClickableBy(By by){
        defaultWait.until(ExpectedConditions.elementToBeClickable(by));
    }

}

package io.shaneconnolly.dev.eeetest.core.pages;

import io.shaneconnolly.dev.eeetest.core.framework.SCWebElement;
import org.springframework.stereotype.Component;

@Component
public class GoogleResultsPage extends BasePage {

    public SCWebElement getResultsSearchTextField() throws Exception {
        return constructCSSElement(
                "#tsf > div:nth-child(2) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input",
                "ResultsSearchField");
    }

    public SCWebElement getResultsTopResultHeader() throws Exception {
        return constructXPathElement(
                "//div[contains(@class, 'Z0LcW')]", "ResultsSearchField");
    }

}


// #tsuid15 > div.ifM9O > div > div.kp-header > div > div.DI6Ufb > div > div > div.Z0LcW
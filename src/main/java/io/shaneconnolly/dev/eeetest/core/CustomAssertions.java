package io.shaneconnolly.dev.eeetest.core;

import io.qameta.allure.Step;
import io.shaneconnolly.dev.eeetest.core.framework.SCWebElement;
import org.springframework.stereotype.Component;
import org.testng.Assert;

@Component
public class CustomAssertions {

    public void assertElementTextEquals(SCWebElement scWebElement, String expectedText, String message) {
        customAssertEquals(scWebElement.getText(), expectedText, message);
    }

    public void assertElementTextNotEquals(SCWebElement scWebElement, String expectedText, String message) {
        customAssertNotEquals(scWebElement.getText(), expectedText, message);
    }

    public void assertTextFieldTextEquals(SCWebElement textField, String expectedText, String message) {
        customAssertEquals(textField.getValue(), expectedText, message);
    }

    public void customAssertEquals(Object actual, Object expected, String message) {
        report(String.format("Assert Equals : actual %s was %s : %s", actual,  expected, message));
        Assert.assertEquals(actual, expected, message);
    }

    public void customAssertNotEquals(Object actual, Object expected, String message) {
        report(String.format("Assert Not Equals : actual %s was not %s : %s", actual,expected, message));
        Assert.assertNotEquals(actual, expected, message);
    }

    @Step
    public void report(String message) {

    }

}

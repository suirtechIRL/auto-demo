package io.shaneconnolly.dev.eeetest.core.pages;


import io.shaneconnolly.dev.eeetest.core.framework.SCWebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class BasePage {

    @Autowired
    WebDriver driver;

    SCWebElement constructCSSElement(String cssLocator, String elementName) throws Exception {
        return constructElement(By.cssSelector(cssLocator), elementName);
    }

    SCWebElement constructIDElement(String locator, String elementName) throws Exception {
        return constructElement(By.id(locator), elementName);
    }

    SCWebElement constructXPathElement(String locator, String elementName) throws Exception {
        return constructElement(By.xpath(locator), elementName);
    }

    private SCWebElement constructElement(By by, String elementName) throws Exception {
        return new SCWebElement(driver, by, elementName);
    }
}

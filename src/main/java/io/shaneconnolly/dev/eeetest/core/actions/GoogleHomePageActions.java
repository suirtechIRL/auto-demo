package io.shaneconnolly.dev.eeetest.core.actions;

import io.qameta.allure.Step;
import io.shaneconnolly.dev.eeetest.core.CustomAssertions;
import io.shaneconnolly.dev.eeetest.core.pages.GoogleHomePage;
import io.shaneconnolly.dev.eeetest.core.pages.GoogleResultsPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GoogleHomePageActions {

    @Autowired
    private WebDriver driver;

    @Autowired
    private GoogleHomePage googleHomePage;

    @Autowired
    private GoogleResultsPage googleResultsPage;

    @Autowired
    private CustomAssertions customAssertions;

    @Step
    public GoogleHomePageActions open(String url) {
        driver.get(url);
        return this;
    }

    @Step
    public GoogleHomePageActions doSearch(String searchString) throws Exception {
        googleHomePage.getSearchTextField().sendKeys(searchString);
        googleHomePage.getSearchTextField().sendKeys(Keys.RETURN);
        return this;
    }

    @Step
    public GoogleHomePageActions verifyResults(String searchTerm) throws Exception {
        customAssertions.assertTextFieldTextEquals
                (googleResultsPage.getResultsSearchTextField(), searchTerm, "Verify Results Have searched Term");
        return this;
    }

    @Step
    public GoogleHomePageActions verifyWinner(String result) throws Exception {
        customAssertions.assertElementTextEquals
                (googleResultsPage.getResultsTopResultHeader(), result, "Verify Search Result Winner");
        return this;
    }

    @Step
    public GoogleHomePageActions verifyNonWinner(String result) throws Exception {
        customAssertions.assertElementTextNotEquals
                (googleResultsPage.getResultsTopResultHeader(), result, "Verify Search Result Non Winner");
        return this;
    }

}

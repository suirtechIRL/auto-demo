package io.shaneconnolly.dev.eeetest.core.framework;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.concurrent.Callable;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

@Data
public class SCWebElement extends RemoteWebElement {

    private Logger logger = (Logger) LogManager.getLogger(SCWebElement.class);
    private static final int MAX_WAIT_TIMEOUT = 5;
    private WebDriverWait webDriverWait;

    private WebDriver driver;

    private String name;
    private By by;
    private WebElement webElement;


    public SCWebElement(WebDriver driver, By by, String name) throws Exception {
        this(driver, by, name, true);
    }

    public SCWebElement(WebDriver driver, By by, String name, boolean waitForVisibility) throws Exception {
        super();
        this.driver = driver;
        this.webDriverWait = new WebDriverWait(driver, MAX_WAIT_TIMEOUT);
        this.by = by;
        this.name = name;

        if (waitForVisibility) this.webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));

        try {
            this.webElement = driver.findElement(this.by);
        } catch (Exception e) {
            // TOTO Deal with exceptions
            throw e;
        }
    }

    @Override
    public void click() {
        logger.info(String.format("clicking Element %s ", this.name));
        this.webElement.click();
    }

    @Override
    public void sendKeys(CharSequence... keysToSend) {
        logger.info(String.format("SendKeys %s to Element [%s] ", Arrays.toString(keysToSend), this.name));
        this.webElement.sendKeys(keysToSend);
    }

    @Override
    public String getText() {
        logger.info(String.format("Get Text of Element [%s]", this.name));
        return this.webElement.getText();
    }

    @Override
    public String getAttribute(String attributeName){
        logger.info(String.format("Get Attribute [%s] of Element %s ", attributeName, this.name));
        return this.webElement.getAttribute(attributeName);
    }

    public boolean waitForTextToEqual(String text){
        await().atMost(20, SECONDS).until(isTextEqual(text));
        return true;
    }

    private Callable<Boolean> isTextEqual(String text){
        return () -> this.webElement.getText().equals(text);
    }

    public void clearAndSendKeys(CharSequence... keysToSend){
        this.webElement.clear();
        this.sendKeys(keysToSend);
    }

    public String getValue(){
        return getAttribute("value");
    }

}

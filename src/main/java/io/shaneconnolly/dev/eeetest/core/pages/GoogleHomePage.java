package io.shaneconnolly.dev.eeetest.core.pages;

import io.shaneconnolly.dev.eeetest.core.framework.SCWebElement;
import org.springframework.stereotype.Component;

@Component
public class GoogleHomePage extends BasePage {

    public SCWebElement getSearchTextField() throws Exception {
        return constructCSSElement("#tsf > div:nth-child(2) > div.A8SBwf > div.RNNXgb >" +
                " div > div.a4bIc > input", "SearchField");
    }

    public SCWebElement getSearchBtn() throws Exception {
        return constructCSSElement("#tsf > div:nth-child(2) > div.A8SBwf.emcav > div.UUbT9 > " +
                "div.aajZCb > div.VlcLAe > center > input.gNO89b", "FeelingLuckyBtn");
    }
}

package io.shaneconnolly.dev.eeetest.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"io.shaneconnolly.dev.eeetest"})
public class WebConfig {

    private ChromeDriver driver;

    @Value("${chrome.driver.path:lib/mac/chromedriver}")
    private String chromeDriverPath;

    @Bean(destroyMethod = "quit")
    public WebDriver chromeDriver() {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        driver = new ChromeDriver();
        return driver;
    }

    @Bean
    public WebDriverWait webDriverWait() {
        return new WebDriverWait(driver, 45);
    }
}

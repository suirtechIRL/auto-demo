package io.shaneconnolly.dev.eeetest.specs;

import io.qameta.allure.Attachment;
import org.apache.logging.log4j.ThreadContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Method;

public class BaseTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebDriver driver;

    @BeforeClass(alwaysRun = true)
    public static void beforeClass() {
        RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
        String pid = rt.getName().split("@")[0];
        ThreadContext.put("PID", pid);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method) {
        ThreadContext.put("TEST", method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        saveScreenShot();
    }

    @Attachment("Screenshot")
    private byte[] saveScreenShot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}

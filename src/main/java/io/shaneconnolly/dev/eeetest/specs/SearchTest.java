package io.shaneconnolly.dev.eeetest.specs;

import io.shaneconnolly.dev.eeetest.config.WebConfig;
import io.shaneconnolly.dev.eeetest.core.actions.GoogleHomePageActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {WebConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SearchTest extends BaseTest {

    @Autowired
    private GoogleHomePageActions googleHomePageActions;

    @Value("${base.url:https://www.google.com/}")
    private String baseUrl;

    @Test(groups = {"smoke", "regression"})
    public void testSearch() throws Exception {
        googleHomePageActions
                .open(baseUrl)
                .doSearch("Rugby World Cup Winners")
                .verifyResults("Rugby World Cup Winners");
    }

    @Test(groups = {"regression"})
    public void testSearchWinner() throws Exception {
        googleHomePageActions
                .open(baseUrl)
                .doSearch("2019 rugby world cup final winner team")
                .verifyWinner("South Africa national rugby union team");
    }

    @Test(groups = {"regression"})
    public void testSearchNonWinner() throws Exception {
        googleHomePageActions
                .open(baseUrl)
                .doSearch("2019 rugby world cup final winner team")
                .verifyNonWinner("England");
    }

    @Test(groups = {"regression"})
    public void testSearchNonWinner2() throws Exception {
        googleHomePageActions
                .open(baseUrl)
                .doSearch("2019 rugby world cup final winner team")
                .verifyNonWinner("Ireland");
    }
}

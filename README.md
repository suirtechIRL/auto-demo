## 888 Sample Automation Project

#### Java Spring TestNG Selenium 

- Project has a number of simple tests to demonstrate Java / TestNG  - Webdriver
- wrote with chrome version 78.0.3904.70 with basic driver capabilities
- Allure Reports ( sample report @ saved-allure-report, cp to webserver to view) 
- if allure command line is installed locally, run : allure serve target/allure-results
- Screenshots and data inputs and outputs in allure steps of report


#### Future Improvemets
- Webdriver is thread safe ( singleton per process, Bean in WebConfig ) 
- parallel execution available via mvn surefire and classes
- add report object to elastic search / kibana for live results

to execute 

###### mac

```bash

mvn clean test

```

###### windows ( not tested )
```bash
mvn -Dchrome.driver.path=lib/win/chromedriver.exe clean test
```


###### serve report ( if allure command line installed locally, after run )
```bash
allure serve target/allure-results
```


###### allure command line
https://docs.qameta.io/allure/#_installing_a_commandline

#### Notes
- Unsure of input keyboad so not added to demo